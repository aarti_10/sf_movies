## SF Movies ##
Create a service that shows on a map where movies have been filmed in San Francisco. The user should be able to filter the view using autocompletion search.
The data is available on [DataSF](http://www.datasf.org/): [Film Locations](https://data.sfgov.org/Arts-Culture-and-Recreation-/Film-Locations-in-San-Francisco/yitu-d5am)

*Source code can be found [here](https://bitbucket.org/aarti_10/sf_movies/src/b55323cac3094983f2a87de6ca91a9be617d0b96?at=master)*

#### Application workflow ####
* The application accepts the movie name as a input. Autocompletion is suggested to user while entering movie name.
* Data for autocompletion is fetched from database.
* After the selection of movie name, all the locations where movie was flimed gets fetched from the database and displayed on the map.

#### Tech Stack ####
- Python 
- Django
- MySQL
- HTML
- JQuery
 
#### Installation ####
- Required packages are listed in requirements.txt. To install them use following command.
    ```
    $  pip install -r requirements.txt
    ```
- To install GeoDjango refer this [document](https://docs.djangoproject.com/en/dev/ref/contrib/gis/).
- To install MySQL Server, refer this [link](https://dev.mysql.com/doc/refman/5.7/en/installing.html). 

#### Configuration ####
- To set the log path directory, add below line in setting.py 
    ```
    $ vim setting.py 
       SITE_ROOT = "PATH to the log directory"
    ```    
#### Database Configuration ####
* Start MySQL server.
    ```
    $ start mysql server 
    ```    
* Log into mysql user.
    ```
    $ mysql -u root -p
    ```    
* Create datbase with name movies_db.
    ```
    mysql>  CREATE DATABASE movies_db;
    ```    
* Create datbase user.
    ```
    mysql>  CREATE USER 'user1'@'localhost' IDENTIFIED BY 'user123';
    ```    
* Give all necessary permissions to the user. 
    ```
    mysql>  GRANT ALL PRIVILEGES ON movies_db.* TO 'user1'@'localhost';
    ```    
### How to run test cases ###
```
$ python manage.py test movies_app.tests
```    
### Deployment instructions ###
* To create the tables in database, use below commands -
    ```
    $ python manage.py makemigrations movies_app
    $ python manage.py migrate
    ```
* To load the data in table movies, use below commands -   
    ```
    $ python manage.py shell
    >>> from movies_app.utils.load_data import load_sf_movies_data
    >>> load_sf_movies_data()
    ```
* To start the application
     ```
    $ python manage.py runserver
    ```
* Verify the application by accessing URL http://127.0.0.1:8000

#### ScreenShot ####
![Screenshot](https://bitbucket.org/aarti_10/sf_movies/src/528d6a7f9899662fe14d09ffac72f1b26996177a/screenshort1.png?at=master)