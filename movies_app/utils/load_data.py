# Stdlib Imports
import csv
import logging

from django.contrib.gis.geos import Point

from movies_app.models import Movies
from movies_app.const import MOVIES_DATA_FILE
from movies_app.utils.common_functions import get_lat_lng


logger = logging.getLogger('exception')


def load_sf_movies_data():
    """ It add  movies data from file to db.
    """
    movies_list = []
    try:
        with open(MOVIES_DATA_FILE) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if row and row['Locations']:
                    lat, lng = get_lat_lng(row['Locations'] + ', San Francisco')
                    if lat and lng:
                        movies_list.append(Movies(
                            title=row['Title'],
                            release_year=row['Release Year'],
                            location=row['Locations'],
                            geom=Point(lng, lat),
                            fun_fact=row['Fun Facts'],
                            production_company=row['Production Company'],
                            distributor=row['Distributor'],
                            director=row['Director'],
                            writer=row['Writer'],
                            actor1=row['Actor 1'],
                            actor2=row['Actor 2'],
                            actor3=row['Actor 3'],
                        ))
            Movies.objects.bulk_create(movies_list, batch_size=1000)
        logger.debug('Added all movies data successfully.')
    except Exception as e:
        logger.debug('Loading of movies data failed.')
        logger.debug(e)


