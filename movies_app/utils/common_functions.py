# -*- coding: utf-8 -*-
# Stdlib imports
import json
import requests
import logging

from movies_app.const import GOOGLE_API_KEY, GOOGLE_API_URL
logger = logging.getLogger('exception')


def get_lat_lng(address):
    """ It will return the lat and lng of the address. It uses googleapis for it.

   Params
       address (str)
           address of location

   Returns
       lat (float)
            lat of the location
       lng (float)
            lng of the location

   """

    url = GOOGLE_API_URL + '?address=' + address + '&key=' + GOOGLE_API_KEY

    response = requests.get(url)
    lat = 0
    lng = 0
    try:
        if response.status_code == 200:
            response_json = json.loads(response.content)
            location = response_json.get('results')[0].get('geometry').get('location')
            lat = location.get('lat', 0)
            lng = location.get('lng', 0)
            logger.debug(address)
    except Exception, e:
        logger.debug(address)
        logger.debug(e)
    return float(lat), float(lng)
