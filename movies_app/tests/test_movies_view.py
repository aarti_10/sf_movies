import json


from django.test import TestCase
from django.test.client import Client
from model_mommy import mommy
from django.contrib.gis.geos import Point
from django.core import serializers


class Movies_View(TestCase):
    def setUp(self):
        self.client = Client()

    def test_load_home_page(self):
        """ It will test home page is rendering or not
        """
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_get_all_movies(self):
        """ It will get all the movies starting with term given
        """

        mommy.make('Movies', title='The Assassination of Richard Nixon', release_year=2004,
                   location='Tosca Caf (242 Columbus Avenue, North Beach)', geom=Point(37.7976266, -122.4058736))
        mommy.make('Movies', title='The Bachelor', release_year=1999,
                   location='Merchant Exchange (465 California Street at Leidesdorff)',
                   geom=Point(37.7925542, -122.4021254))
        mommy.make('Movies', title='The Birds', release_year=2008, location='Powell and Geary Streets (Union Square)',
                   geom=Point(37.7873819,-122.4082412))
        mommy.make('Movies', title='The Bridge', release_year=2006, location='Golden Gate Bridge',
                   geom=Point(37.8199286, -122.4782551))
        response = self.client.get('/get_movies/?term=Th')
        response_json = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response_json), 4)

    def test_no_movies_with_term(self):
        """ It will check the scenerio where no movies start with given term
        """

        response = self.client.get('/get_movies/?term=Ra')
        response_json = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response_json), 0)

    def test_movies_with_one_location(self):
        """ It will check the movies with given name and gives the location
        """
        mommy.make('Movies', title='The Bridge', release_year=2006, location='Golden Gate Bridge',
                   geom=Point(37.8199286, -122.4782551))
        response = self.client.post('/get_locations/', json.dumps({'movie': 'The Bridge'}),
                                    content_type="application/json")
        response_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertIn('features', response_json)
        self.assertIn('type', response_json)
        self.assertIn('crs', response_json)
        self.assertEqual(len(response_json['features']), 1)

    def test_movies_with_more_than_one_location(self):
        """ It will check the movies with given name and gives all the 3 location
        """
        mommy.make('Movies', title='The Bachelor', release_year=1999,
                   location='Merchant Exchange (465 California Street at Leidesdorff)',
                   geom=Point(37.7925542, -122.4021254))
        mommy.make('Movies', title='The Bachelor', release_year=1999,
                   location='Washington Square Park (North Beach)',
                   geom=Point(37.8008473, -122.4099791))
        mommy.make('Movies', title='The Bachelor', release_year=1999,
                   location='Sutter-Stockton Garage (330 Sutter Street at Stockton)',
                   geom=Point(37.7900691, -122.4064769))
        response = self.client.post('/get_locations/', json.dumps({'movie': 'The Bachelor'}),
                                    content_type="application/json")
        response_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertIn('features', response_json)
        self.assertIn('type', response_json)
        self.assertIn('crs', response_json)
        self.assertEqual(len(response_json['features']), 3)

    def test_movies_with_no_location(self):
        """ It will check the movies has location specification in db and if not found returns the geojson
        with 0 features
        """
        response = self.client.post('/get_locations/', json.dumps({'movie': 'The Bachelor'}),
                                    content_type="application/json")
        response_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertIn('features', response_json)
        self.assertIn('type', response_json)
        self.assertIn('crs', response_json)
        self.assertEqual(len(response_json['features']), 0)