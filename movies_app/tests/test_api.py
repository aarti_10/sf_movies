import json

from django.test import TestCase
from movies_app.utils.common_functions import get_lat_lng

class GOOGLE_API_TEST(TestCase):

    def setUp(self):
        self.address1 = 'Westin St. Francis Hotel (335 Powell Street, Union Square)'
        self.address2 = 'Former Hall of Justice (750 Kearny Street at Washington)'
        self.lat1 = 37.7877881
        self.lng1 = -122.4088321

    def test_get_lat_lng_correct_address(self):
        """ The api gives lat lng for correct address format
        """
        lat, lng = get_lat_lng(self.address1)

        self.assertEqual(lat, self.lat1)
        self.assertEqual(lng, self.lng1)


    def test_get_lat_lng_incorrect_address_format(self):
        """ The api gives lat lng as 0 for incorrect address format
        """
        lat, lng = get_lat_lng(self.address2)

        self.assertEqual(lat, 0)
        self.assertEqual(lng, 0)
