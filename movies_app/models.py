# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Stdlib imports
import datetime

# Django imports
from django.db import models
from django.contrib.gis.db import models as gismodels


class Movies(gismodels.Model):
    """
        It stores the information of the movies and its shoot location
    """
    title = models.CharField(max_length=255)
    release_year = models.IntegerField(default=0)
    location = models.CharField(max_length=500)
    geom = gismodels.PointField(null=True)
    fun_fact = models.TextField()
    production_company = models.CharField(max_length=300)
    director = models.CharField(max_length=255)
    distributor = models.CharField(max_length=255)
    writer = models.CharField(max_length=500)
    actor1 = models.CharField(max_length=255)
    actor2 = models.CharField(max_length=255)
    actor3 = models.CharField(max_length=255)

    objects = gismodels.GeoManager()

    def __unicode__(self):
        return self.title

    def to_dict(self):
        data = {
            'title': self.title,
            'release_year': self.release_year,
            'location': self.location,
            'geom': (self.geom.x, self.geom.y),
            'fun_fact': self.fun_fact,
            'production_company': self.production_company,
            'director': self.director,
            'distributor': self.distributor,
            'writer': self.writer,
            'actor1': self.actor1,
            'actor2': self.actor2,
            'actor3': self.actor3,
        }
        return data

    class Meta:
        db_table = 'movies'
        app_label = 'movies_app'
