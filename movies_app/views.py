import json
import logging

# Django imports
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader

from movies_app.models import Movies
from django.core.serializers import serialize

logger = logging.getLogger('exception')


@csrf_exempt
def show_home(request):
    """ It will show the maps on the home page

    Params
        request (Request)
            HTTP request

    Returns
        HTTP Response

    """
    t = loader.get_template('movies_app/index.html')
    data = Movies.objects.all()
    c = {'data': data}
    response = HttpResponse(t.render(c))
    return response


@csrf_exempt
def get_movies(request):
    """ It get all the movies from db starting with given term

   Params
       request (Request)
           HTTP request

   Returns
       HTTP Response

   """
    logger.debug('GET')
    if request.method == 'GET':
        term = request.GET.get('term', '')
        logger.debug(term)
        # search by term the movie name contains
        # movies = Movies.objects.filter(title__icontains=term).values('title').distinct()[:20]
        # search by term the movie name start
        movies = Movies.objects.filter(title__istartswith=term).values('title').distinct()[:20]
        logger.debug(movies)
        results = []
        for index, movie in enumerate(movies):
            movie_json = dict()
            movie_json['id'] = index
            movie_json['label'] = movie['title']
            movie_json['value'] = movie['title']
            results.append(movie_json)
        data = json.dumps(results)

    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


@csrf_exempt
def get_locations(request):
    """ It will get all the shoot location of movies from db and return the geojson of locations.

   Params
       request (Request)
           HTTP request

   Returns
       HTTP Response

   """
    logger.debug('POST')
    if request.method == 'POST':
        data = json.loads(request.body)
        # search by term the movie name contains
        # movies = Movies.objects.filter(title__icontains=term).values('title').distinct()[:20]
        # search by term the movie name start
        locations = Movies.objects.filter(title=data["movie"])
        serialize_locations = serialize('geojson', locations, geometry_field='geom', fields=('fun_fact', 'location'))
        data = serialize_locations
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)
